#if !defined(SHADOWS_INCLUDED)
	#define SHADOWS_INCLUDED

	//Includes 
	#include "UnityCG.cginc"


	//Common data structs
	struct appdata
	{
		float4 vertex : POSITION;
		float3 normal : NORMAL;
	};


	//For calculating point light shadows
	#if defined(SHADOWS_CUBE)
		struct v2f
		{
			float4 vertex : SV_POSITION;
			float3 lightVec : TEXCOORD0;
		};

		v2f ShadowVertex(appdata v)
		{
			v2f o;
			o.vertex = UnityObjectToClipPos(v.vertex);

			//Calculate light->vertex vector using Unity provided variable (declared in UnityShaderVariables.cginc)			
			o.lightVec = mul(unity_ObjectToWorld, v.vertex).xyz - _LightPositionRange.xyz;

			return o;
		}

		float4 ShadowFragment(v2f i) : SV_TARGET
		{
			//Calculate depth from interpolated lightVec & Unity provided shadow bias
			float depth = length(i.lightVec) + unity_LightShadowBias.x;

			//.w component stores the reciprocal of lights range, this is equivalent to: depth/light range
			depth *= _LightPositionRange.w;

			//Unity function to encode depth into cube-map values around the pointlight
			return UnityEncodeCubeShadowDepth(depth);
		}

	//For calculating dir/spot light shadows
	#else		
		float4 ShadowVertex(appdata v) : SV_POSITION
		{
			//Unity function to calculate clip position with shadow bias applied
			float4 position = UnityClipSpaceShadowCasterPos(v.vertex, v.normal);

			//Output depth values directly to SV_POSITION render target
			return UnityApplyLinearShadowBias(position);
		}

		float4 ShadowFragment() : SV_TARGET
		{
			//Does not need to return anything to render target
			return 0;
		}
	#endif



#endif