﻿Shader "Unlit/PrintingShaderNoComments"
{
    Properties
    {	
					_WireThickness ("Wire Thickness", Range(0.0, 2)) = 1
					_WireColour ("Wire Colour", Color) = (0.5, 0.5, 0.5, 0.5)
					_ScanlineColour ("Scan Line Colour", Color) = (1  , 0  , 0  , 1  )
[PowerSlider(3.0)]	_ScanlineSpeed  ("Scan Line Speed", Range(0, 10)) = 1		
[PowerSlider(3.0)]	_ScanlinePrecision ("Scan Line Precision", Range(1, 100)) = 1
					_ScanLineHeight ("Scan Line Height", Range(0,1)) = 1
					_ModelsHeight ("Models Height", float) = 1
[MaterialToggle]    _HeightCheckMode ("Height Check Mode", float) = 0
    }
    SubShader
    {
        Tags { "Queue"="Transparent" "RenderType"="Transparent" }
        LOD 100
		
		Cull Off
		ZWrite Off

        Pass 
        {
			blend SrcAlpha OneMinusSrcAlpha						
			

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag          
			#pragma geometry geom

            #include "UnityCG.cginc"		

            struct appdata
            {
                float4 vertex : POSITION;
            };

            struct v2g
            {
			    float4 vertex	: SV_POSITION;
				float4 worldPos : TEXCOORD0;
				float4 localPos : TEXCOORD1;
            };

			struct g2f
			{
				float4 vertex	: SV_POSITION;
				float3 bary		: TEXCOORD0;
				float4 localPos : TEXCOORD1;
			};

			float	_WireThickness;
            float4	_WireColour;
			float	_ScanLineHeight;
			float	_ModelsHeight;

            v2g vert (appdata v)
            {
                v2g o;
                o.vertex = UnityObjectToClipPos(v.vertex);           				
				o.worldPos = mul(unity_ObjectToWorld, v.vertex);
				o.localPos = v.vertex;
                return o;
            }
		
			[maxvertexcount(3)]
			void geom (triangle v2g input[3], inout TriangleStream<g2f> triStream)
			{								
				g2f o;
				
				o.vertex = input[0].vertex;
				o.bary   = float3(1, 0, 0);
				o.localPos = input[0].localPos;
				
				triStream.Append(o);
				
				o.vertex = input[1].vertex;
				o.bary   = float3(0, 1, 0);		
				o.localPos = input[1].localPos;
				triStream.Append(o);

				o.vertex = input[2].vertex;
				o.bary   = float3(0, 0, 1);
				o.localPos = input[2].localPos;
				triStream.Append(o);
			}

            fixed4 frag (g2f i) : SV_Target
            {								
				float delta = fwidth(min(i.bary.x, min(i.bary.y, i.bary.z))) * _WireThickness;
								
              			
				if (!any(bool3(i.bary.x < delta, i.bary.y < delta, i.bary.z < delta)))
					discard;
				
								
				float transparencyMask = 1 - (i.localPos.y / _ModelsHeight < _ScanLineHeight);				
				return float4(_WireColour.rgb, transparencyMask);
            }
            ENDCG
        }


		Pass 
		{		
			blend SrcAlpha OneMinusSrcAlpha			
			
			
			

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
				float4 localPos : TEXCOORD0;
			};

			float4	_ScanlineColour;
			float	_ScanlinePrecision;
			float	_ScanlineSpeed;
			float	_ModelsHeight;
			float	_HeightCheckMode;
			float	_ScanLineHeight;

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.localPos = v.vertex;
				return o;
			}

			fixed4 frag (v2f i) : SV_Target
			{							
				float normalizedY = saturate(i.localPos.y / _ModelsHeight); 
				float lineMoveFactor = _Time.y * _ScanlineSpeed;						       								
				float linePositionY = _ScanLineHeight;						
				
				
				float proximityToLine = saturate(1-abs(normalizedY - linePositionY) * _ScanlinePrecision);				
				
				float lineAlpha = smoothstep( 0, 1, proximityToLine);
				
				if (_HeightCheckMode)					
					return float4(normalizedY + proximityToLine, 
								saturate(normalizedY - proximityToLine), 
								saturate(normalizedY - proximityToLine), 
								1);

				float4 scanLine = float4(_ScanlineColour.rgb, lineAlpha);
				return scanLine;
			}


			ENDCG
		}
    }
}
