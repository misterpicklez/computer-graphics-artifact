﻿Shader "Unlit/ScanningShaderNoComments"
{
    Properties
    {	
					_WireThickness ("Wire Thickness", Range(0.0, 2)) = 1
					_WireColour ("Wire Colour", Color) = (0.5, 0.5, 0.5, 0.5)
					_ScanlineColour ("Scan Line Colour", Color) = (1  , 0  , 0  , 1  )
[PowerSlider(3.0)]	_ScanlineSpeed  ("Scan Line Speed", Range(0, 10)) = 1		
[PowerSlider(3.0)]	_ScanlinePrecision ("Scan Line Precision", Range(1, 100)) = 1
					_ModelsHeight ("Models Height", float) = 1
[MaterialToggle]    _HeightCheckMode ("Height Check Mode", float) = 0
    }
    SubShader
    {
        Tags { "Queue"="Transparent" "RenderType"="Transparent" }
        LOD 100
		Cull Off

        Pass //Wireframe pass
        {
			ZWrite Off
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag          
			#pragma geometry geom

            #include "UnityCG.cginc"		

            struct appdata
            { 
                float4 vertex : POSITION;
            };

            struct v2g
            {
			    float4 vertex	: SV_POSITION;
				float4 worldPos : TEXCOORD0;
            };

			struct g2f
			{
				float4 vertex	: SV_POSITION;
				float3 bary		: TEXCOORD0;
			};

			float	_WireThickness;
            float4	_WireColour;
			

            v2g vert (appdata v)
            {
                v2g o;
                o.vertex = UnityObjectToClipPos(v.vertex);           				
				o.worldPos = mul(unity_ObjectToWorld, v.vertex);
                return o;
            }

			//No return type - need to define max vertices that can be returned
			[maxvertexcount(3)]
			void geom (triangle v2g input[3], inout TriangleStream<g2f> triStream)
			{				
				//Create typical output struct 
				g2f o;

				//Pass each triangle vertex along with same position
				//but with baryocentric masses attached
				o.vertex = input[0].vertex;
				o.bary   = float3(1, 0, 0);

				//don't return as that can only output 1 vertex maximum, instead append
				//to inout stream which can output 1 or more to fragment shader 
				triStream.Append(o);

				//Repeat with all other triangle vertices
				o.vertex = input[1].vertex;
				o.bary   = float3(0, 1, 0);		
				triStream.Append(o);

				o.vertex = input[2].vertex;
				o.bary   = float3(0, 0, 1);
				triStream.Append(o);
			}

            fixed4 frag (g2f i) : SV_Target
            {				
				//Screen-space derivative samples the rate of change in the barycentric coordinates
				//to get a consistent baseline multiplied by _WireThickness
				float delta = fwidth(min(i.bary.x, min(i.bary.y, i.bary.z))) * _WireThickness;
								

                //No frag calculations for blending barycentric coords necessary as its 
				//performed during interpolation stage. compare with delta to discard frags				
				if (!any(bool3(i.bary.x < delta, i.bary.y < delta, i.bary.z < delta)))
					discard;

				//Otherwise return _WireColour
				return _WireColour;
            }
            ENDCG
        }


		Pass //Scanline pass
		{			
			Blend SrcAlpha OneMinusSrcAlpha
			ZWrite Off

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
				float4 localPos : TEXCOORD0;
			};

			float4	_ScanlineColour;
			float	_ScanlinePrecision;
			float	_ScanlineSpeed;
			float	_ModelsHeight;
			float	_HeightCheckMode;

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.localPos = v.vertex;
				return o;
			}

			fixed4 frag (v2f i) : SV_Target
			{				
				//Calculate the frags normalized height & scan lines position
				float normalizedY = saturate(i.localPos.y / _ModelsHeight); //0 to 1
				float lineMoveFactor = _Time.y * _ScanlineSpeed;						       								
				float linePositionY = abs(sin(lineMoveFactor));				//0 to 1
				

				//Calculate alpha depending on proximity to linescan & _ScanlinePrecision
				float proximityToLine = saturate(1-abs(normalizedY - linePositionY) * _ScanlinePrecision);				

				//Smoothstep to get an alpha value fading out from the line
				float lineAlpha = smoothstep( 0, 1, proximityToLine);
				
				if (_HeightCheckMode)					
					return float4(normalizedY + proximityToLine, 
								saturate(normalizedY - proximityToLine), 
								saturate(normalizedY - proximityToLine), 
								1);

				float4 scanLine = float4(_ScanlineColour.rgb, lineAlpha);
				return scanLine;
			}


			ENDCG
		}
    }
}
