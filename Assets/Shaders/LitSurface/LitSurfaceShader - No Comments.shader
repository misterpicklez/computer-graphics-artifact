﻿Shader "Unlit/LitSurfaceShaderNoComments"
{
    Properties
    {
		_Tint ("Tint", Color) = (1, 1, 1, 1)
        _Albedo ("Albedo", 2D) = "white" {}

		_Metallic ("Metallic", Range(0, 1)) = 0
		_Smoothness ("Smoothness", Range(0, 1)) = 0.5

		[NoScaleOffset]
		_NormalMap ("Normals", 2D) = "bump" {}
		_BumpScale ("Bump Scale", float) = 1
		
		_DetailTex ("Detail Texture", 2D) = "gray" {}
		_DetailNormalMap ("Detail Normals", 2D) = "bump" {}
		_DetailBumpScale ("Detail Bump Scale", float) = 1 
    }

	CGINCLUDE
	#define BINORMAL_PER_FRAGMENT
	ENDCG

    SubShader
    {           
        Pass 
        {			
			Tags {"LightMode" = "ForwardBase"}

            CGPROGRAM
			#pragma target 3.0			
			#pragma multi_compile _ SHADOWS_SCREEN
			#pragma multi_compile _ VERTEXLIGHT_ON
						
            #pragma vertex vert
            #pragma fragment frag            
			
            #define FORWARD_BASE_PASS

			#include "../Lighting.cginc"
			
            ENDCG
        }

		Pass 
		{
			
			Tags {"LightMode" = "ForwardAdd"}
			Blend One One	
			ZWrite Off		

			CGPROGRAM

			#pragma multi_compile_fwdadd_fullshadows
			
			#pragma vertex vert
			#pragma fragment frag

			#include "../Lighting.cginc"

			ENDCG
		}

		Pass 
		{		
			Tags{"LightMode" = "ShadowCaster"}

			CGPROGRAM

			#pragma multi_compile_shadowcaster

			#pragma vertex ShadowVertex
			#pragma fragment ShadowFragment

			#include "../Shadows.cginc"
			ENDCG
		}
    }
}
