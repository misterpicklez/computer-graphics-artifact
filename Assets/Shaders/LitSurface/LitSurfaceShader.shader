﻿Shader "Unlit/LitSurfaceShader"
{
    Properties
    {
		_Tint ("Tint", Color) = (1, 1, 1, 1)
        _Albedo ("Albedo", 2D) = "white" {}

		_Metallic ("Metallic", Range(0, 1)) = 0
		_Smoothness ("Smoothness", Range(0, 1)) = 0.5

		[NoScaleOffset]
		_NormalMap ("Normals", 2D) = "bump" {}
		_BumpScale ("Bump Scale", float) = 1
		
		_DetailTex ("Detail Texture", 2D) = "gray" {}
		_DetailNormalMap ("Detail Normals", 2D) = "bump" {}
		_DetailBumpScale ("Detail Bump Scale", float) = 1 
    }

	CGINCLUDE
	#define BINORMAL_PER_FRAGMENT
	ENDCG

    SubShader
    {           
        Pass //Base pass (directional light & vertex lighting calculated)
        {
			//LightMode tag informs Unity which pass to call depending on the light source being calculated for
			//Forward base draws main directional light & vertex lights https://docs.unity3d.com/Manual/SL-PassTags.html
			Tags {"LightMode" = "ForwardBase"}

            CGPROGRAM
			#pragma target 3.0
			//multi compile commands to create two variants, one with the keyword defined the other without -- https://docs.unity3d.com/Manual/SL-MultipleProgramVariants.html			
			#pragma multi_compile _ SHADOWS_SCREEN
			#pragma multi_compile _ VERTEXLIGHT_ON
			
			//Define which function vertex/fragment shader use
            #pragma vertex vert
            #pragma fragment frag            

			//Always defined for base pass, no need for multi-compile directives
            #define FORWARD_BASE_PASS

			//Reusable .cginc for lighting calculations
			#include "../Lighting.cginc"
			
            ENDCG
        }

		Pass //Additive pass (additional lights & shadow receiving calculated)
		{
			//Forward add calls once per light that affects the object and calculates its effect including depth testing for any shadow received
			Tags {"LightMode" = "ForwardAdd"}
			Blend One One	//additive blending, adds result to previous pass(es)
			ZWrite Off		//Disable writing to depth buffer (already calculated in base pass)

			CGPROGRAM

			//multi compile for common light types (dir, spot, point). Also defines SHADOW_SCREEN
			#pragma multi_compile_fwdadd_fullshadows
			//equivalent to:
			//#pragma multi_compile DIRECTIONAL DIRECTIONAL_COOKIE POINT POINT_COOKIE SPOT 
			//#pragma multi_compile __ SHADOW_SCREEN

			#pragma vertex vert
			#pragma fragment frag

			#include "../Lighting.cginc"

			ENDCG
		}

		Pass //Depth pass for calculating shadow map
		{
			//ShadowCaster mode informs Unity that this is a depth pass for shadow casting purposes (calculate depths from lights POV)
			Tags{"LightMode" = "ShadowCaster"}

			CGPROGRAM

			//multi compile for defining SHADOWS_CUBE or SHADOWS_DEPTH based on if pass is ran for point light or dir/spot light
			#pragma multi_compile_shadowcaster
			//equivalent to:
			//#pragma multi_compile SHADOWS_DEPTH SHADOWS_CUBE

			#pragma vertex ShadowVertex
			#pragma fragment ShadowFragment

			#include "../Shadows.cginc"
			ENDCG
		}
    }
}
