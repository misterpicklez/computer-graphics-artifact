﻿Shader "Unlit/HologramShader"
{
    Properties
    {
				 _Color ("Colour", Color) = (1, 1, 1, 1) 
				 _ScrollSpeed ("Scroll Speed", Range(0, 100)) = 1
				 _LineDensity ("Line Density", Range(0, 1)) = 1
				 _LineThickness ("Line Thickness", Range(-1, 2)) = 1
[MaterialToggle] _ScrollDirection ("Scroll Direction", float) = 1
[MaterialToggle] _ScreenSpaceLines ("Screen Space Lines?", float) = 1
				 _EdgeGlowStrength ("Edge Glow Strength", Range(0, 3)) = 1
				 _LineGlowStrength ("Line Glow Strength", Range(0, 3)) = 1
				 _ModelsHeight ("Models Height (y axis)", float) = 1
	}
    SubShader
    {
        Tags { "Queue"="Transparent" "RenderType"="Transparent" }
        LOD 100
		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha//[Src * A +- Dst * B]
		Cull Off

        Pass
        {			
            CGPROGRAM
			#pragma target 2.5
            #pragma vertex vert
            #pragma fragment frag    

            #include "UnityCG.cginc"

			float4	_Color;
			float	_ScrollSpeed;
			float	_LineDensity;
			float	_LineThickness;
			float	_ScrollDirection;
			float	_ScreenSpaceLines;
			float	_EdgeGlowStrength;
			float	_LineGlowStrength;
			float	_ModelsHeight;

            struct appdata
            {
                float4 vertex	: POSITION;
				float3 normal	: NORMAL;
            };

            struct v2f
            {                               
                float4 vertex	: SV_POSITION;
				float4 vertexWorldPos : TEXCOORD1;
				float4 vertexModelPos : TEXCOORD2;
				float3 normal	: TEXCOORD3;
            };
            

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.vertexWorldPos = mul(unity_ObjectToWorld, v.vertex);
				o.vertexModelPos = v.vertex;
                o.normal = mul(unity_ObjectToWorld, v.normal);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {				
				//############### Scrolling lines effect ###############
				//Calculate world & screen space y-position of the fragment
				float modelYPos = i.vertexModelPos.y / (_ModelsHeight * 0.01);
				float screenYPos = i.vertex.y; 												
				
				//Calculate cosArg as either world or screen space based y coordinate
				float cosArg = (_ScreenSpaceLines * screenYPos) + 
							   ((1-_ScreenSpaceLines) * modelYPos);

				//Reset range of _ScrollDirection from 0/1 to -1/1
				float scrollDirection = (_ScrollDirection * 2) - 1;

				//Calculate movement factor using time, _ScrollDirection & _ScrollSpeed
				float scrollFactor = _Time.y * scrollDirection * _ScrollSpeed;

				//Use Cosine to oscillate an alpha value based on:
				//cosArg		 - chosen coordinate space y-value (changes across a surface)
				//_LineDensity	 - affects frequency of wave/how often alpha goes from 1->0->1
				//_LineThickness - constant offset to all alpha values 
				//scrollFactor   - offset to cosArg that changes over time at constant rate
				float alphaWave = max(0, cos(cosArg * _LineDensity + scrollFactor) + _LineThickness);

				//Saturate to remove negative alpha values & apply glow strength
				alphaWave = saturate(alphaWave) * _LineGlowStrength;
				

				//############### Edge/Fresnel glow effect ###############				
				//Calculate the view direction
				float3 camPos = _WorldSpaceCameraPos;
				float3 camToFrag = i.vertexWorldPos - camPos;
				float3 viewDir = normalize(camToFrag);

				//Need to re-normalize normal value since it length can vary after the interpolation process
                float3 normal = normalize(i.normal);

				//absolute dot product gives a viewing angle aproximation
				//this determines how close to the edge a fragment is
				float viewAngle = pow(abs(dot(viewDir, normal)), _EdgeGlowStrength);
				float edgeProximity = 1 - viewAngle; 
								


				//Output both scrolling lines and edge glow added
				float finalAlpha = saturate(alphaWave + edgeProximity);
                return float4(_Color.xyz, finalAlpha);
            }
            ENDCG
        }
    }
}
