﻿Shader "Unlit/DisassembleShaderNoComments"
{
    Properties
    {
		_Tint ("Tint", Color) = (1, 1, 1, 1)
        _Albedo ("Albedo", 2D) = "white" {}

		_Metallic ("Metallic", Range(0, 1)) = 0
		_Smoothness ("Smoothness", Range(0, 1)) = 0.5

		[NoScaleOffset]
		_NormalMap ("Normals", 2D) = "bump" {}
		_BumpScale ("Bump Scale", float) = 1
		
		_DetailTex ("Detail Texture", 2D) = "gray" {}
		_DetailNormalMap ("Detail Normals", 2D) = "bump" {}
		_DetailBumpScale ("Detail Bump Scale", float) = 1

		_Timeline ("Timeline", Range(0,1)) = 0
		_PlaySpeed ("Play Speed", Range(0, 5)) = 1
		_ExplodeOffset ("Explosion Offset", float) = 1	
		_CenterOffset ("Center Offset", Vector) = (0, 0, 0)
    }

	CGINCLUDE
	#define BINORMAL_PER_FRAGMENT
	ENDCG

    SubShader
    {		
        Pass 
        {
			Tags {"LightMode" = "ForwardBase"}
			Cull Off

            CGPROGRAM			
			#pragma target 4.0 
			#pragma multi_compile _ SHADOWS_SCREEN
			#pragma multi_compile _ VERTEXLIGHT_ON
			
            #pragma vertex vert
			#pragma geometry geom
            #pragma fragment frag            

            #define FORWARD_BASE_PASS
			#define GEOMETRY_DISASSEMBLE

			#include "../Lighting With Geometry.cginc"
			#include "../Disassemble Geometry.cginc"

            ENDCG
        }

		Pass 
		{			
			Tags {"LightMode" = "ForwardAdd"}
			Blend One One	
			ZWrite Off		

			CGPROGRAM
			#pragma target 4.0
			#pragma multi_compile_fwdadd_fullshadows
			
			#pragma vertex vert
			#pragma geometry geom
			#pragma fragment frag

			#define GEOMETRY_DISASSEMBLE

			#include "../Lighting With Geometry.cginc"
			#include "../Disassemble Geometry.cginc"
			 
			ENDCG
		}
		Pass 
		{		
			Tags{"LightMode" = "ShadowCaster"}

			CGPROGRAM
			
			#pragma multi_compile_shadowcaster			

			#pragma vertex ShadowVertex
			#pragma fragment ShadowFragment

			#include "../Shadows.cginc"
			ENDCG
		}		
    }
}
