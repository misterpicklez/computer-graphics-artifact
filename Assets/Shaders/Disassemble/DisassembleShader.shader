﻿Shader "Unlit/DisassembleShader"
{
    Properties
    {
		//-----Lit surface properties-----
		_Tint ("Tint", Color) = (1, 1, 1, 1)
        _Albedo ("Albedo", 2D) = "white" {}

		_Metallic ("Metallic", Range(0, 1)) = 0
		_Smoothness ("Smoothness", Range(0, 1)) = 0.5

		[NoScaleOffset]
		_NormalMap ("Normals", 2D) = "bump" {}
		_BumpScale ("Bump Scale", float) = 1
		
		_DetailTex ("Detail Texture", 2D) = "gray" {}
		_DetailNormalMap ("Detail Normals", 2D) = "bump" {}
		_DetailBumpScale ("Detail Bump Scale", float) = 1

		//-----Disassemble properties-----
		_Timeline ("Timeline", Range(0,1)) = 0
		_PlaySpeed ("Play Speed", Range(0, 5)) = 1
		_ExplodeOffset ("Explosion Offset", float) = 1	
		_CenterOffset ("Center Offset", Vector) = (0, 0, 0)
    }

	CGINCLUDE
	#define BINORMAL_PER_FRAGMENT
	ENDCG

    SubShader
    {		
        Pass //Base pass (directional light & vertex lighting calculated)
        {
			Tags {"LightMode" = "ForwardBase"}
			Cull Off

            CGPROGRAM
			//SM 4.0 needed for geometry shader
			#pragma target 4.0 
			#pragma multi_compile _ SHADOWS_SCREEN
			#pragma multi_compile _ VERTEXLIGHT_ON
			
            #pragma vertex vert
			#pragma geometry geom
            #pragma fragment frag            

            #define FORWARD_BASE_PASS
			#define GEOMETRY_DISASSEMBLE

			#include "../Lighting With Geometry.cginc"
			#include "../Disassemble Geometry.cginc"

            ENDCG
        }

		Pass //Additive pass (additional lights & shadow receiving calculated)
		{			
			Tags {"LightMode" = "ForwardAdd"}
			Blend One One	//additive blending, adds result to previous pass(es)
			ZWrite Off		//Disable writing to depth buffer (already calculated in base pass)

			CGPROGRAM
			#pragma target 4.0
			#pragma multi_compile_fwdadd_fullshadows
			
			#pragma vertex vert
			#pragma geometry geom
			#pragma fragment frag

			#define GEOMETRY_DISASSEMBLE

			#include "../Lighting With Geometry.cginc"
			#include "../Disassemble Geometry.cginc"
			 
			ENDCG
		}
		Pass //Depth pass for calculating shadow map
		{
			//ShadowCaster mode informs Unity that this is a depth pass for shadow casting purposes (calculate depths from lights POV)
			Tags{"LightMode" = "ShadowCaster"}

			CGPROGRAM

			//multi compile for defining SHADOWS_CUBE or SHADOWS_DEPTH based on if pass is ran for point light or dir/spot light
			#pragma multi_compile_shadowcaster
			//equivalent to:
			//#pragma multi_compile SHADOWS_DEPTH SHADOWS_CUBE

			#pragma vertex ShadowVertex
			#pragma fragment ShadowFragment

			#include "../Shadows.cginc"
			ENDCG
		}		
    }
}
