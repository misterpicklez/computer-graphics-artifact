#if !defined(LIGHTING_INCLUDED)
#define LIGHTING_INCLUDED		


//These will also include UnityStandardBRDF, UnityStandardUtils and UnityCG .cginc files
#include "UnityPBSLighting.cginc"
#include "AutoLight.cginc"


//Shader parameters
float4 _Tint;
float4 _Albedo_ST, _DetailTex_ST;
float _Metallic;
float _Smoothness;
sampler2D _Albedo, _DetailTex;
sampler2D _NormalMap, _DetailNormalMap;
float _BumpScale, _DetailBumpScale;


//Data structures
struct appdata {
	float4 vertex	: POSITION; //must be named 'vertex' to be compatible with Unity macro code
	float4 tangent	: TANGENT;
	float3 normal	: NORMAL;	
	float2 uv		: TEXCOORD0;
};

struct v2f {
	float4 pos		: SV_POSITION; //must be named 'pos' to be compatible with Unity macro code
	float4 uv		: TEXCOORD0;
	float3 normal	: TEXCOORD1;
	float3 worldPos	: TEXCOORD2;

	//Shader can be configured for different settings depending on which keywords are defined.
	//define data needed for calculating binormals per fragment vs per vertex
	#if defined(BINORMAL_PER_FRAGMENT)
		float4 tangent : TEXCOORD3; 
	#else
		float3 tangent : TEXCOORD3;
		float3 binormal : TEXCOORD4;
	#endif

	//Unity macro from AutoLight.cginc, defines shadow coordinate interpolators if Unity detects
	//shadows are enabled on this object. parameter 5 = TEXCOORD5
	SHADOW_COORDS(5)

	//define vertex light colour if cheap vertex lighting is turned on
	#if defined(VERTEXLIGHT_ON)
		float3 vertexLightColor : TEXCOORD6;
	#endif	
};


//Helper functions
float3 CreateBinormal(float3 normal, float3 tangent, float binormalSign)
{
	return cross(normal, tangent.xyz) * (binormalSign * unity_WorldTransformParams.w);
}
void ComputeVertexLightColor(inout v2f i)
{
	#if defined(VERTEXLIGHT_ON)
		//Built in UnityCG function to calc the effects of all 4 point lights on this vertex
		i.vertexLightColor = Shade4PointLights(
			unity_4LightPosX0, unity_4LightPosY0, unity_4LightPosZ0,
			unity_LightColor[0].rgb, unity_LightColor[1].rgb,
			unity_LightColor[2].rgb, unity_LightColor[3].rgb,
			unity_4LightAtten0, i.worldPos, i.normal
		);
	#endif
}
UnityLight CreateLight(v2f i) {
	UnityLight light;
	//Use keyword definitions to determine which shader pass this is (point light or directional light)
	#if defined(POINT) || defined(POINT_COOKIE) || defined(SPOT)
		light.dir = normalize(_WorldSpaceLightPos0.xyz - i.worldPos);
	#else
		light.dir = _WorldSpaceLightPos0.xyz;
	#endif

	//We can use this macro with interpolator now since i contains a shadowCoordinate interpolated value
	UNITY_LIGHT_ATTENUATION(attenuation, i, i.worldPos);	

	//float3 lightVec = _WorldSpaceLightPos0.xyz - i.worldPos;
	light.color = _LightColor0.rgb * attenuation;
	light.ndotl = DotClamped(i.normal, light.dir);
	return light;
}
UnityIndirect CreateIndirectLight(v2f i)
{
	UnityIndirect indirectLight;
	indirectLight.diffuse = 0;
	indirectLight.specular = 0;

	#if defined(VERTEXLIGHT_ON)
		indirectLight.diffuse = i.vertexLightColor; //If vertex lighting is on, use this colour for indirect light
	#endif

	//Add spherical harmonics light to light calculation (very cheap)
	#if defined(FORWARD_BASE_PASS)
		indirectLight.diffuse += max(0, ShadeSH9(float4(i.normal, 1)));
	#endif

	return indirectLight;
}
void InitialiseFragmentNormal(inout v2f i)
{		
	//Using Unity utility functions to unpack normals with their respective scaling
	float3 mainNormal = UnpackScaleNormal(tex2D(_NormalMap, i.uv.xy), _BumpScale);
	float3 detailNormal = UnpackScaleNormal(tex2D(_DetailNormalMap, i.uv.zw), _DetailBumpScale); 
	
	//Need binormal to convert to tangent space so normals avoid any axis alignment dependencies
	float3 tangentSpaceNormal = BlendNormals(mainNormal, detailNormal); 
	#if defined(BINORMAL_PER_FRAGMENT)
		float3 binormal = CreateBinormal(i.normal, i.tangent.xyz, i.tangent.w);
	#else
		float3 binormal = i.binormal;
	#endif	 

	//With the three axis for tangent space (normal:V, tangent:U, binormal:W) we can calculate normal in this new space
	//This is done by multiplying each component along its axis of alignment 
	i.normal = normalize(
		tangentSpaceNormal.x * i.tangent +	//(x, 0, 0)
		tangentSpaceNormal.y * binormal +	//(0, y, 0)
		tangentSpaceNormal.z * i.normal		//(0, 0, z)
	);	
}


//Vertex and Fragment Shaders
v2f vert(appdata v) 
{
	v2f i;

	//Typical object to X transformations
	#if defined(GEOMETRY_DISASSEMBLE)
		i.pos = v.vertex; //calculate clip space in geo shader
	#else
		i.pos = UnityObjectToClipPos(v.vertex); //calculate clip space here
	#endif
	i.worldPos = mul(unity_ObjectToWorld, v.vertex);
	i.normal = normalize(UnityObjectToWorldNormal(v.normal));	


	#if defined(BINORMAL_PER_FRAGMENT)
		//Only calculate tangent (binormal will not be interpolated)
		i.tangent = float4(UnityObjectToWorldDir(v.tangent.xyz), v.tangent.w);
	#else
		//Calculate tangent and binormal per vertex (binormal will be interpolated)
		i.tangent = UnityObjectToWorldDir(v.tangent.xyz);
		i.binormal = CreateBinormal(i.normal, i.tangent, v.tangent.w);
	#endif
	

	//Stores a pair of UV channels in one UV interpolator, one for albedo & one for detail texture
	i.uv.xy = TRANSFORM_TEX(v.uv, _Albedo); 
	i.uv.zw = TRANSFORM_TEX(v.uv, _DetailTex); 


	#if !defined(GEOMETRY_DISASSEMBLE)
		//Macro from AutoLight.cginc, converts shadow coordinates to project space coordinates (0-1 range)
		TRANSFER_SHADOW(i); 
		//Calculate vertex lighting (if enabled)
		ComputeVertexLightColor(i);	
	#endif
	
	return i;
}


fixed4 frag(v2f i) : SV_TARGET{
	//Put the interpolated normal into tangent space (so it follows the curve of the surface)
	InitialiseFragmentNormal(i);

	//Calculate/set up variables that go into Unity PBS BRDF
	float3 viewDir = normalize(_WorldSpaceCameraPos - i.worldPos);
	float3 albedo = tex2D(_Albedo, i.uv.xy).rgb * _Tint.rgb; //sample texture and mul by tint property
	albedo *= tex2D(_DetailTex, i.uv.zw) * unity_ColorSpaceDouble;
	float3 specularTint;
	float oneMinusReflectivity;
	albedo = DiffuseAndSpecularFromMetallic(albedo, _Metallic, specularTint, oneMinusReflectivity);
			
	//Create the light structs that feed into BRDF, large reusable code bodies so split into helper functions
	UnityLight light = CreateLight(i);
	UnityIndirect indirect = CreateIndirectLight(i);

	//Call Unity PBS BRDF using variables set up
	return UNITY_BRDF_PBS(
		albedo, specularTint,
		oneMinusReflectivity, _Smoothness,
		i.normal, viewDir,
		light, indirect
	);	
}

#endif