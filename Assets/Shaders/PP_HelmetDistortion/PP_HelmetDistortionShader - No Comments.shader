﻿Shader "Hidden/PP_HelmetDistortionShaderNoComments"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		_BarrelPower ("Barrel Power", float) = 1.0
    }
    SubShader
    {
        Cull Off ZWrite Off ZTest Always

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"
			
			uniform float _BarrelPower;

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

			float2 distort(float2 pos)
			{				
				float theta = atan2(pos.y, pos.x);
				float radius = length(pos);

				radius = pow(radius, _BarrelPower);
				
				float adj, op;
				sincos(theta, op, adj); 
				pos.x = radius * adj;
				pos.y = radius * op;			

				return 0.5 * (pos + 1.0);
			}

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            sampler2D _MainTex;

            fixed4 frag (v2f i) : SV_Target
            {				
				float2 xy = 2.0 * i.uv - 1; 				
				
                float2 uv = distort(xy);
                				
				float4 col = tex2D(_MainTex, uv);
                return col;
            }
            ENDCG
        }
    }
}
