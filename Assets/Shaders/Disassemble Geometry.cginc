#if !defined(DISASSEMBLE_GEOMETRY_INCLUDED)
#define DISASSEMBLE_GEOMETRY_INCLUDED		


//Shader parameters
float _Timeline;
float _PlaySpeed;
float _ExplodeOffset;
float3 _CenterOffset;



//Geometry shader
[maxvertexcount(3)]
void geom (triangle v2f input[3], inout TriangleStream<v2f> triStream)
{	
	//Calculate the center of the triangle in local pos (outward trajectory per triangle)	
	float3 triCenter = ( (input[0].pos - _CenterOffset) +		
						 (input[1].pos - _CenterOffset) +		
						 (input[2].pos - _CenterOffset) )/3;	
	
	triCenter = normalize(triCenter);

	//Update timeline and offset vertices accordingly
	float timeline = saturate(_Timeline + max(0, sin(_Time.y * _PlaySpeed)));
		
	//Calculate offset (same value per vertex in a triangle)
	float4 offsetAmount = float4(timeline * _ExplodeOffset * triCenter.xyz, 0);

	v2f o;
	//For each vertex in the triangle
	for (int i = 0; i < 3; i++)
	{		
		//Copy all data along but with offset
		o = input[i];		
		o.pos += offsetAmount;		
		o.worldPos = mul(unity_ObjectToWorld, o.pos);
		

		//Then calculate SV_POSITION and worldPos with offset applied		
		o.pos = UnityObjectToClipPos(o.pos);
		

		//Calculate shadows and vertex light colours 		 
		//TRANSFER_SHADOW(o);	
		ComputeVertexLightColor(o);
		
		

		triStream.Append(o);
	}
}


#endif