﻿Shader "Unlit/ForceFieldShader"
{
    Properties
    {
		_Color("Color", Color) = (1, 1, 1, 1)		
        _NormalMap ("Normal", 2D) = "bump" {}
		_EdgeThickness ("Edge Thickness", Range(0,1)) = 0.1
		_EdgeFadeAmount ("Edge Fade Amount", Range(0,2)) = 1
		_ShimmerSpeed ("Shimmer Speed", float) = 1
		_InnerOpacity ("Inner Opacity", Range(0,1)) = 0.25
		_EdgeOpacity ("Edge Opacity", Range(0,1)) = 1
		_Glossiness ("Glossiness", Range(0, 10)) = 1
    }
    SubShader
    {
        Tags { 
			"Queue"="Transparent" 
			"RenderType"="Transparent" 			
		}
        LOD 100
		
		Blend SrcAlpha OneMinusSrcAlpha //[src.A +- dst.B] <--- src is fragment output, dst is fragment behind it. 
										//With this blend mode A = SrcAlpha, B = OneMinusSrcAlpha
										//so...
										//src.SrcAlpha + dst.(1-SrcAlpha)
										//A and B will be inversely proportional, their total combined must equal 1
										//e.g: src.0.25 + dst.0.75
										//The src alpha becomes a weight for how much it is visibly overlayed on the dst
        Pass
        {
			Tags{ "LightMode" = "ForwardBase" }
            CGPROGRAM
			#pragma target 3.0
            #pragma vertex vert
            #pragma fragment frag
            

            #include "UnityCG.cginc"
			#include "UnityLightingCommon.cginc"
			#include "AutoLight.cginc"
			#include "UnityPBSLighting.cginc" //Allows us to use BRDF function

			fixed4		_Color;
			sampler2D	_NormalMap;
			float4		_NormalMap_ST;  
			float		_EdgeThickness;
			float		_EdgeFadeAmount;
			float		_ShimmerSpeed;
			float		_InnerOpacity;
			float		_EdgeOpacity;
			float		_Glossiness;

            struct VertexInput
            {
                float4 vertex : POSITION;				
				float3 normal : NORMAL;
				float4 tangent : TANGENT;				
                float2 uv : TEXCOORD0;
            };

            struct VertexOutput
            {
                float2 uv : TEXCOORD0;               
                float4 vertex : SV_POSITION;
				float3 worldPos : TEXCOORD1;

				//tangent space per fragment
				half3 tangentSpaceXAxis : TEXCOORD2; //tangent.x, bitangent.x, normal.x
				half3 tangentSpaceYAxis : TEXCOORD3; //tangent.y, bitangent.y, normal.y
				half3 tangentSpaceZAxis : TEXCOORD4; //tangent.z, bitangent.z, normal.z
            };

			          
           

            VertexOutput vert (VertexInput v)
            {
                VertexOutput o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _NormalMap);
				o.worldPos = mul(unity_ObjectToWorld, v.vertex);
				
				//Need to provide a per-vertex world normal, tangent & bitangent
				half3 wNormal = UnityObjectToWorldNormal(v.normal);
				half3 wTangent = UnityObjectToWorldDir(v.tangent.xyz);
				half tangentSign = v.tangent.w * unity_WorldTransformParams.w;
				half3 wBitangent = cross(wNormal, wTangent) * tangentSign;
				o.tangentSpaceXAxis = half3(wTangent.x, wBitangent.x, wNormal.x);
				o.tangentSpaceYAxis = half3(wTangent.y, wBitangent.y, wNormal.y);
				o.tangentSpaceZAxis = half3(wTangent.z, wBitangent.z, wNormal.z);

                return o;
            }

            fixed4 frag (VertexOutput i) : SV_Target
            {					
				//############### Alpha for inner and edge of force field ###############
				//UV coords tell how close a fragment is to the edge of a quad
				//UV range is:    0---->1 
				//Adjusted range: 1->0->1   (0=center; 1= edge)
				float2 adjustedRange = abs((i.uv * 2) - 1); 
				float proximityToEdge = max(adjustedRange.x, adjustedRange.y);

				//Determine if fragment is within threshold of _EdgeThickness
				//0=inner , 1=edge				
				float isEdge = step(1 - _EdgeThickness, proximityToEdge); 
				
				//Use interpolation to determine the amount this fragment should fade
				//depending on edge thickness & fade amount				
				float edgeFade = smoothstep(1-_EdgeThickness, 1, proximityToEdge * _EdgeFadeAmount);				
				float edgeAlpha = min(_EdgeOpacity, isEdge*edgeFade);

				//Calculate alpha, for edges this will include a positive value for edgeAlpha
				//for inner fragments edgeAlpha will be 0, leaving just _InnerOpacity
				float alpha = min(1, edgeAlpha + _InnerOpacity); 
				


				//############### Force field movement ##################################
				//delta changes over time with a speed multiplier
				float delta = _Time.x * _ShimmerSpeed;				
				float sinDelta, cosDelta;
				sincos(delta, sinDelta, cosDelta);
				sinDelta = (sinDelta + 1) * 0.5f;
				cosDelta = (cosDelta + 1) * 0.5f;

				//Sample normal map twice, with 2 UVs panning different speed, direction & offsets   
				fixed3 tNormal = UnpackNormal( tex2D(_NormalMap, float2(delta, sinDelta) + i.uv + float2(0.1, -0.1)) ) +
								UnpackNormal( tex2D(_NormalMap, float2(-delta, cosDelta) + i.uv) );
				tNormal = tNormal/2;

				//Dot product between normal & tangent-space axes to get tangent-space normals 
				//i.e. normals that correctly curve around the surface
				half3 worldNormal;
				worldNormal.x = dot(i.tangentSpaceXAxis, tNormal);
				worldNormal.y = dot(i.tangentSpaceYAxis, tNormal);
				worldNormal.z = dot(i.tangentSpaceZAxis, tNormal);



				//############### Calculate lighting ##################################	
				//Gather directional light info
				float3 lightDir = _WorldSpaceLightPos0.xyz;
				float3 lightColour = _LightColor0.rgb;

				//Calculate view & reflection vectors
				float3 viewDir = normalize(_WorldSpaceCameraPos - i.worldPos);
				float3 viewReflect = reflect(-viewDir, worldNormal);

				//Setup for using Unity built in BRDF
				float3 albedo = float3(_Color.xyz);		
				float3 specularTint = float3(1, 1, 1) * _Glossiness;
				float oneMinusReflectivity = 1 - 1; 
				float oneMinusRoughness = 1 - 0;						

				UnityLight light;
				light.color = lightColour;
				light.dir = lightDir;
				light.ndotl = saturate(dot(worldNormal, lightDir));
				
				UnityIndirect indirectLight;
				indirectLight.diffuse = 0.0;
				indirectLight.specular = 0.0;

				//Calculate light dir dependent colour with Unity PBS-BRDF, this includes specular highlights  
				float3 frag = UNITY_BRDF_PBS(
					albedo, specularTint,
					oneMinusReflectivity, oneMinusRoughness,
					worldNormal, viewDir,
					light, indirectLight
				);	
				//Reverse light and addativly calculate to colour the other side
				light.dir = -light.dir;
				frag += UNITY_BRDF_PBS(
					albedo, specularTint,
					oneMinusReflectivity, oneMinusRoughness,
					worldNormal, viewDir,
					light, indirectLight
				);							

				//Calculate emissive colour based on viewing angle
				float ambientAmount = 1-saturate(dot(viewReflect, viewDir));
				float3 ambientColour = _Color * ambientAmount ;
				float ambientWeight = 0.5;
				float specularWeight = 1-ambientWeight;
				return float4((ambientColour * ambientWeight) + (frag * specularWeight), alpha);
				
				//return float4(frag.xyz, alpha); //##### REMOVED #####
            }
            ENDCG
        }
    }
}



