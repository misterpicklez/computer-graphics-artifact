﻿Shader "Unlit/ForceFieldShaderNoComments"
{
    Properties
    {
		_Color("Color", Color) = (1, 1, 1, 1)		
        _NormalMap ("Normal", 2D) = "bump" {}
		_EdgeThickness ("Edge Thickness", Range(0,1)) = 0.1
		_EdgeFadeAmount ("Edge Fade Amount", Range(0,2)) = 1
		_ShimmerSpeed ("Shimmer Speed", float) = 1
		_InnerOpacity ("Inner Opacity", Range(0,1)) = 0.25
		_EdgeOpacity ("Edge Opacity", Range(0,1)) = 1
		_Glossiness ("Glossiness", Range(0, 10)) = 1
    }
    SubShader
    {
        Tags { 
			"Queue"="Transparent" 
			"RenderType"="Transparent" 			
		}
        LOD 100
				
		ZWrite Off	
		Blend SrcAlpha OneMinusSrcAlpha 

        Pass
        {
			Tags{ "LightMode" = "ForwardBase" }
            CGPROGRAM
			#pragma target 3.0
            #pragma vertex vert
            #pragma fragment frag
            

            #include "UnityCG.cginc"
			#include "UnityLightingCommon.cginc"
			#include "AutoLight.cginc"
			#include "UnityPBSLighting.cginc" //Allows us to use BRDF function

			fixed4		_Color;
			sampler2D	_NormalMap;
			float4		_NormalMap_ST;  
			float		_EdgeThickness;
			float		_EdgeFadeAmount;
			float		_ShimmerSpeed;
			float		_InnerOpacity;
			float		_EdgeOpacity;
			float		_Glossiness;

            struct VertexInput
            {
                float4 vertex : POSITION;				
				float3 normal : NORMAL;
				float4 tangent : TANGENT;				
                float2 uv : TEXCOORD0;
            };

            struct VertexOutput
            {
                float2 uv : TEXCOORD0;               
                float4 vertex : SV_POSITION;
				float3 worldPos : TEXCOORD1;
			
				half3 tangentSpaceXAxis : TEXCOORD2; 
				half3 tangentSpaceYAxis : TEXCOORD3; 
				half3 tangentSpaceZAxis : TEXCOORD4; 
            };

			          
           

            VertexOutput vert (VertexInput v)
            {
                VertexOutput o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _NormalMap);
				o.worldPos = mul(unity_ObjectToWorld, v.vertex);
								
				half3 wNormal = UnityObjectToWorldNormal(v.normal);
				half3 wTangent = UnityObjectToWorldDir(v.tangent.xyz);
				half tangentSign = v.tangent.w * unity_WorldTransformParams.w;
				half3 wBitangent = cross(wNormal, wTangent) * tangentSign;
				o.tangentSpaceXAxis = half3(wTangent.x, wBitangent.x, wNormal.x);
				o.tangentSpaceYAxis = half3(wTangent.y, wBitangent.y, wNormal.y);
				o.tangentSpaceZAxis = half3(wTangent.z, wBitangent.z, wNormal.z);

                return o;
            }

            fixed4 frag (VertexOutput i) : SV_Target
            {									
				float2 adjustedRange = abs((i.uv * 2) - 1); 
				float proximityToEdge = max(adjustedRange.x, adjustedRange.y);
					
				float isEdge = step(1 - _EdgeThickness, proximityToEdge); 
											
				float edgeFade = smoothstep(1-_EdgeThickness, 1, proximityToEdge * _EdgeFadeAmount);				
				float edgeAlpha = min(_EdgeOpacity, isEdge*edgeFade);
				
				float alpha = min(1, edgeAlpha + _InnerOpacity); 
				

				
				float delta = _Time.x * _ShimmerSpeed;				
			                  
				fixed3 tNormal = UnpackNormal( tex2D(_NormalMap, float2(-delta, -delta) + i.uv + float2(0.1, -0.1)) ) +
								UnpackNormal( tex2D(_NormalMap, float2(delta, delta) + i.uv) );				
				tNormal = tNormal/2;
				
				half3 worldNormal;
				worldNormal.x = dot(i.tangentSpaceXAxis, tNormal);
				worldNormal.y = dot(i.tangentSpaceYAxis, tNormal);
				worldNormal.z = dot(i.tangentSpaceZAxis, tNormal);


				
				float3 lightDir = _WorldSpaceLightPos0.xyz;
				float3 lightColour = _LightColor0.rgb;
				
				float3 viewDir = normalize(_WorldSpaceCameraPos - i.worldPos);
				float3 viewReflect = reflect(-viewDir, worldNormal);
				
				float3 albedo = float3(_Color.xyz);		
				float3 specularTint = float3(1, 1, 1) * _Glossiness;
				float oneMinusReflectivity = 1 - 1; 
				float oneMinusRoughness = 1 - 0;						

				UnityLight light;
				light.color = lightColour;
				light.dir = lightDir;
				light.ndotl = saturate(dot(worldNormal, lightDir));
				
				UnityIndirect indirectLight;
				indirectLight.diffuse = 0.0;
				indirectLight.specular = 0.0;
				
				float3 frag = UNITY_BRDF_PBS(
					albedo, specularTint,
					oneMinusReflectivity, oneMinusRoughness,
					worldNormal, viewDir,
					light, indirectLight
				);					
				light.dir = -light.dir;
				frag += UNITY_BRDF_PBS(
					albedo, specularTint,
					oneMinusReflectivity, oneMinusRoughness,
					worldNormal, viewDir,
					light, indirectLight
				);							
				
				float ambientAmount = 1-saturate(dot(viewReflect, viewDir));
				float3 ambientColour = _Color * ambientAmount ;
				float ambientWeight = 0.5;
				float specularWeight = 1-ambientWeight;
				return float4((ambientColour * ambientWeight) + (frag * specularWeight), alpha);								
            }
            ENDCG
        }
    }
}



