﻿Shader "Unlit/PortalShader"
{
    Properties
    {
[NoScaleOffset]  _MainTex ("Texture", 2D) = "white" {}
				 _Color ("Color", Color) = (1, 1, 1, 1)
				 _TransparencyMultiplier ("Transparency Multiplier", Range(0, 2)) = 1
				 _RotationSpeed ("Rotation Speed", Range(0, 10) ) = 1
[MaterialToggle] _RotationDirection ("Clockwise", float ) = 1
				 _DistortionStrength ("Distortion Strength", Range(0, 1)) = 1
    }
    SubShader
    {
        Tags { "Queue"="Transparent" "RenderType"="Transparent" }
        LOD 100
		Blend SrcAlpha OneMinusSrcAlpha
		Cull Off

		//Preceding pass to grab the texture drawn behind the portal
		//and stores it in _Background sampler2D variable
		GrabPass {
			"_BackgroundTexture"
		}

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag           

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex	: POSITION;
                float2 uv		: TEXCOORD0;
            };

            struct v2f
            {                  
                float4 vertex	: SV_POSITION;
				float2 uv		: TEXCOORD0;   
				float4 grabPos	: TEXCOORD1;
            };

			sampler2D   _BackgroundTexture;
            sampler2D	_MainTex;
            float4		_MainTex_ST;
			float4		_Color;
			float		_TransparencyMultiplier;
			float		_RotationSpeed;
			float		_RotationDirection;
			float		_DistortionStrength;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);

				//Reset _RotationDirection from 0/1 to -1/1, negate so it matches description (clockwise = 1, counter = -1)
				float rotationDirection = -1 * ((_RotationDirection * 2) - 1);

				//Create a rotation matrix
				float sinTheta, cosTheta; 
				sincos(_RotationSpeed * _Time.y * rotationDirection, sinTheta, cosTheta);				
				float2x2 rotationMatrix = float2x2(cosTheta, -sinTheta, //[ cosθ -sinθ ] counter-clockwise 
												   sinTheta, cosTheta); //[ sinθ  cosθ ] rotation matrix
																							 
																							 				
				//Reset the UV range from 0/1 to -1/1 to change origin to center
				float2 originUVs = (v.uv * 2) - 1;

				//Rotate UVs (around origin)
				float2 rotatedUV = mul( originUVs, rotationMatrix);								

				//reset range back to 0/1
				rotatedUV.xy *= 0.5;
				rotatedUV.xy += 0.5; 
                								
				o.uv = rotatedUV;		
				
				o.grabPos = ComputeGrabScreenPos(o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {									
				//############### Alpha circle cutout ###################
				//Re-center UV around 0,0 then get length
				float2 distFromCenter = length((i.uv * 2) - 1);

				//Alpha value is 0 when outside the circle that fits in -1/1 range
				float transparencyMask = step(distFromCenter, 1);
				if (!transparencyMask)
					discard;


				//############### Colour and transparency ###############
                //Sample texture for colour and alpha values
				float4 sampleValue = tex2D(_MainTex, i.uv);
                float3 colour = sampleValue.rgb * _Color;
				
				//Transparency is encoded in rgb channels, picked r arbitrarily (b or g work the same)
				float alpha = saturate((sampleValue.r) * _TransparencyMultiplier);// * transparencyMask);
				
				
				//############### Distortion ############################
				//Get a parameter that changes depending on rotation from center
				float angle = dot(float2(0, 1), (i.uv * 2) - 1);

				//Create a distortion vector based on the angle & _DistortionStrength
				float3 distortion = angle * _DistortionStrength * 0.5;

				//Sample the frag behind portal & project it onto the surface with distortion
				float3 background = tex2Dproj(_BackgroundTexture, float4(i.grabPos.xyz + distortion, i.grabPos.w)).xyz;
				
				//Create f4 colours for portal and distorted background with a combined weight of 1
				//based on the brightness of the sampled pixel. Portal colours rgb value is the lowest
				//between it and alpha to prevent rgb components spilling over when added if alpha=0
				float4 colour4	   = (sampleValue.r)   * float4(min(colour.rgb,alpha),	alpha);
				float4 distortion4 = (1-sampleValue.r) * float4(background.rgb       ,	1);// * transparencyMask);
							
				//!!! Another method: gives clear distortions with no sample-based wrinkles !!!
				//float4 distortion4 = (transparencyMask) * float4(background.rgb, 1);

				//Add the background and the portal with a combined weight of 1				
				float4 final = colour4 + distortion4;		
				return final;
            }
            ENDCG
        }
    }
}
