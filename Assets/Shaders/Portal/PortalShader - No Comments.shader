﻿Shader "Unlit/PortalShaderNoComments"
{
    Properties
    {
[NoScaleOffset]  _MainTex ("Texture", 2D) = "white" {}
				 _Color ("Color", Color) = (1, 1, 1, 1)
				 _TransparencyMultiplier ("Transparency Multiplier", Range(0, 2)) = 1
				 _RotationSpeed ("Rotation Speed", Range(0, 10) ) = 1
[MaterialToggle] _RotationDirection ("Clockwise", float ) = 1
				 _DistortionStrength ("Distortion Strength", Range(0, 1)) = 1
    }
    SubShader
    {
        Tags { "Queue"="Transparent" "RenderType"="Transparent" }
        LOD 100
		Blend SrcAlpha OneMinusSrcAlpha
		Cull Off
	
		GrabPass {
			"_BackgroundTexture"
		}

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag           

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex	: POSITION;
                float2 uv		: TEXCOORD0;
            };

            struct v2f
            {                  
                float4 vertex	: SV_POSITION;
				float2 uv		: TEXCOORD0;   
				float4 grabPos	: TEXCOORD1;
            };

			sampler2D   _BackgroundTexture;
            sampler2D	_MainTex;
            float4		_MainTex_ST;
			float4		_Color;
			float		_TransparencyMultiplier;
			float		_RotationSpeed;
			float		_RotationDirection;
			float		_DistortionStrength;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
				
				float rotationDirection = -1 * ((_RotationDirection * 2) - 1);

				
				float sinTheta, cosTheta; 
				sincos(_RotationSpeed * _Time.y * rotationDirection, sinTheta, cosTheta);				
				float2x2 rotationMatrix = float2x2(cosTheta, -sinTheta, 
												   sinTheta, cosTheta); 
																							 
																							 							
				float2 originUVs = (v.uv * 2) - 1;
				
				float2 rotatedUV = mul( originUVs, rotationMatrix);								
				
				rotatedUV.xy *= 0.5;
				rotatedUV.xy += 0.5; 
                								
				o.uv = rotatedUV;		
				
				o.grabPos = ComputeGrabScreenPos(o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {									
				//############### Alpha circle cutout ###################				
				float2 distFromCenter = length((i.uv * 2) - 1);			
				float transparencyMask = step(distFromCenter, 1);
				if (!transparencyMask)
					discard;


				//############### Colour and transparency ###############               
				float4 sampleValue = tex2D(_MainTex, i.uv);
                float3 colour = sampleValue.rgb * _Color;								
				float alpha = saturate((sampleValue.r) * _TransparencyMultiplier);
				
				
				//############### Distortion ############################			
				float angle = dot(float2(0, 1), (i.uv * 2) - 1);				
				float3 distortion = angle * _DistortionStrength * 0.5;				
				float3 background = tex2Dproj(_BackgroundTexture, float4(i.grabPos.xyz + distortion, i.grabPos.w)).xyz;								
				float4 colour4	   = (sampleValue.r)   * float4(min(colour.rgb,alpha),	alpha);
				float4 distortion4 = (1-sampleValue.r) * float4(background.rgb       ,	1);
													
				float4 final = colour4 + distortion4;		
				return final;
            }
            ENDCG
        }
    }
}
