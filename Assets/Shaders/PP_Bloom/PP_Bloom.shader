﻿Shader "Hidden/PP_Bloom"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
    }

	//Shared data common to each pass
	CGINCLUDE
		#include "UnityCG.cginc"
		sampler2D _MainTex, _SourceTex;
		float4 _MainTex_TexelSize;
		half4 _Filter;
		half _Intensity;

		struct VertexData {
			float4 vertex	: POSITION;
			float2 uv		: TEXCOORD0;
		};

		struct Interpolators {
			float4 pos		: SV_POSITION;
			float2 uv		: TEXCOORD0;
		};

		//Helper functions
		half3 Prefilter(half3 c)
		{		
			half3 brightness = max(c.r, max(c.g, c.b)); //get brightest of RGB components
			half soft = brightness - _Filter.x;
			soft = clamp(soft, 0, _Filter.y);
			soft = soft * soft * _Filter.z;	//Calculate a soft curve to soften cutoff in max() below
			half contribution = max(soft, brightness - _Filter.w);
			contribution /= max(brightness, 0.000001); //ensure no division by 0

			//Colour = (brightness - threshold)/brightness, 
			//when t is 0, colour remains 1
			//as t increases, colour drops towards 0 when b=t
			return c * contribution;
		}
		half3 Sample(float2 uv)
		{
			//Sample only RGB from source image 
			return tex2D(_MainTex, uv).rgb;
		}
		half3 SampleBox(float2 uv, float delta)
		{
			//Box sampling: 4 samples of adjacent 2x2 pixel blocks get averaged, then averaged between those results
			float4 o = _MainTex_TexelSize.xyxy * float2(-delta, delta).xxyy; //(x size * -delta, y size * -delta, x size * delta, y size * delta)
			half3 s = 
				Sample(uv + o.xy) + Sample(uv + o.zy) +
				Sample(uv + o.xw) + Sample(uv + o.zw);
			return s * 0.25f;
		}

		//Vertex shader
		Interpolators vert (VertexData v)
		{
			Interpolators i;
			i.pos = UnityObjectToClipPos(v.vertex);
			i.uv = v.uv;
			return i;
		}
	ENDCG

    SubShader
    {
        // No culling or depth
        Cull Off ZWrite Off ZTest Always
		
		Pass //0: Prefilter downsample pass - Set pixels that don't meet threshold as black
		{	
            CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag

				half4 frag (Interpolators i) : SV_Target
				{
					return half4(Prefilter(SampleBox(i.uv, 1)), 1);
				}
           
            ENDCG
        }

        Pass //1: Downsampling - box sample delta of 1
		{	
            CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag

				half4 frag (Interpolators i) : SV_Target
				{
					return half4(SampleBox(i.uv, 1), 1);
				}
           
            ENDCG
        }

		Pass //2: Upsampling - box sample delta of 0.5
		{	
			Blend One One
            CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag

				half4 frag (Interpolators i) : SV_Target
				{
					return half4(SampleBox(i.uv, 0.5), 1);
				}
           
            ENDCG
        }

		Pass //3: Final upsampling - box sample delta of 0.5 added to source texture
		{				
            CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag

				half4 frag (Interpolators i) : SV_Target
				{
					//Sample source (with accumulated processing)
					half4 c = tex2D(_SourceTex, i.uv);

					//Add final upsample
					c.rgb += _Intensity * SampleBox(i.uv, 0.5);
					return c;
				}
           
            ENDCG
        }

		Pass //4: Debug pass - perform last upsample and output on its own
		{
			CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				
				half4 frag (Interpolators i) : SV_Target
				{
					return half4(_Intensity * SampleBox(i.uv, 0.5), 1);
				}
			ENDCG
		}
    }
}
