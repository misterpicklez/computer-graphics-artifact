﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ShaderConsole
{
    E_NONE,
    E_FORCEFIELD,
    E_HOLOGRAM,
    E_PORTAL,
    E_SCANNING,
    E_PRINTING,
    E_DISASSEMBLE,
    E_MAX
}

public class ShaderDriver : MonoBehaviour
{
    public ShaderConsole shaderConsole;
    

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
