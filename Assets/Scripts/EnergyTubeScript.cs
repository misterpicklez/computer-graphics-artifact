﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EnergyTubeScript : MonoBehaviour
{
    public Material TubeMat;
    private static float offset = 0;
    [Range(-10, 10)]
    public float speed = 1;

    // Update is called once per frame
    void Update()
    {
        //Constantly pan the albedo texture across
        
        offset += Time.deltaTime * speed ;
        TubeMat.SetTextureOffset("_MainTex", new Vector2(0, offset));
    }
}
