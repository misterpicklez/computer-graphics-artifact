﻿using System;
using UnityEngine;

[ExecuteInEditMode, ImageEffectAllowedInSceneView]
public class BloomEffect : MonoBehaviour
{
    [Range(0, 10)]
    public float intensity = 1;
    [Range(1, 16)]    
    public int iterations = 1;
    public Shader bloomShader;
    [Range(0, 10)]
    public float threshold = 1;
    public bool debug;
    [Range(0, 1)]
    public float softThreshold = 0.5f;

    [NonSerialized]
    Material bloomMat;

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {   
        //Instantiate bloom shader material if null
        if (bloomMat == null){
            bloomMat = new Material(bloomShader);
            bloomMat.hideFlags = HideFlags.HideAndDontSave;
        }        
        //Pre-compute values that are constant per pass to reduce redundant calculations (more work for CPU but is negligible)
        Vector4 filter;
        float knee = threshold * softThreshold;        
        filter.x = threshold - knee;
        filter.y = 2f * knee;
        filter.z = 0.25f / (knee + 0.00001f);
        filter.w = threshold;
        bloomMat.SetVector("_Filter", filter);
        bloomMat.SetFloat("_Intensity", Mathf.GammaToLinearSpace(intensity));
        
        const int BoxDownPrefilterPass = 0;
        const int BoxDownPass = 1;
        const int BoxUpPass = 2;
        const int FinalBloomPass = 3;
        const int DebugBloomPass = 4;

        //Use array to keep track of all (potential) render sampling render textures
        RenderTexture[] textures = new RenderTexture[16];

        //Create temp render texture for applying downsampling to
        int width = source.width / 2;
        int height = source.height / 2;
        RenderTextureFormat format = source.format;        
        RenderTexture currentDestination = textures[0] = RenderTexture.GetTemporary(
            source.width, source.height, 0, source.format
        );

        //Initial downsample pass, then set current pass as result
        Graphics.Blit(source, currentDestination, bloomMat, BoxDownPrefilterPass);       
        RenderTexture currentSource = currentDestination;


        //############### LOOP SAMPLING ITERATIONS ###############
        //Use progressive sampling (halve width-height across multiple passes) rather 
        //than directly using a larger kernel as that results in discarded pixel data 
        int i = 1;
        //Progressive downsampling loop
        for (; i < iterations; i++)
        {
            width /= 2;
            height /= 2;
            if (width < 2 || height < 2)
                break; 
            
            currentDestination = textures[i] = RenderTexture.GetTemporary(width, height, 0, format);

            //Iterative downsample pass
            Graphics.Blit(currentSource, currentDestination, bloomMat, BoxDownPass);            
            currentSource = currentDestination;
        }
        //Progressive upsampling loop
        for (i -= 2; i >= 0; i--)
        {
            currentDestination = textures[i];
            textures[i] = null;

            //Iterative upsample pass
            Graphics.Blit(currentSource, currentDestination, bloomMat, BoxUpPass);
            RenderTexture.ReleaseTemporary(currentSource);
            currentSource = currentDestination;
        }
        //#######################################################        

        if (debug)
            Graphics.Blit(currentSource, destination, bloomMat, DebugBloomPass);
        else
        {
            //Final upsample pass needs the processed source to add to final box sample pass
            bloomMat.SetTexture("_SourceTex", source);

            //Final upsample pass
            Graphics.Blit(currentSource, destination, bloomMat, FinalBloomPass);            
        }
        RenderTexture.ReleaseTemporary(currentSource);
    }
}
