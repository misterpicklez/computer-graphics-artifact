﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatingHologramScript : MonoBehaviour
{
    public float m_BobAmount = 1;
    public float m_BobSpeed = 1;
    public float m_RotateSpeed = 1;

    private float m_Timer = 0;
    private float m_InitialHeight;
    

    private void Start()
    {
        m_InitialHeight = transform.position.y;
    }

    // Update is called once per frame
    void Update()
    {
        m_Timer += Time.deltaTime;                
        float t_BobOffset = Mathf.Abs(Mathf.Sin(m_Timer * m_BobSpeed));      

        float t_Bob = Mathf.Lerp(m_InitialHeight, m_InitialHeight + m_BobAmount, t_BobOffset);
        transform.position = new Vector3(transform.position.x, t_Bob, transform.position.z);
        transform.RotateAround(transform.position, new Vector3(0, 1, 0), m_RotateSpeed);
    }
}
