﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode, ImageEffectAllowedInSceneView]
public class HelmetDistortionEffect : MonoBehaviour
{
    [Range(0, 1)]
    public float barrelPower = 1;
    public Shader distortShader;
    Material distortMat;
    

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        //Instantiate distortion material if null
        if (distortMat == null)
        {
            distortMat = new Material(distortShader); 
            distortMat.hideFlags = HideFlags.HideAndDontSave;
        }
        distortMat.SetFloat("_BarrelPower", 1 - barrelPower);

        //Invoke distortion shader
        Graphics.Blit(source, destination, distortMat);
    }
}
 