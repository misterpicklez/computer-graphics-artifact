﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PrintingConsole : ConsoleScript
{
    public Material Material2;
    [Range(0, 2)]
    public float PlaySpeed = 1;
    private float Timeline = 0;
    private bool Play = false;
    private bool Direction = true; //true - go up, false - go down


    // Start is called before the first frame update
    protected override void Start()
    {
        //Setup options for console        
        option tOption = new option(OptionType.E_BOOL, new Vector4(0, 0, 0, 0), new Vector4(0, 0, 0, 0), new Vector4(1, 0, 0, 0), "");
        options.Add(tOption);


        base.Start();


        //Initialize default material properties
        Material.SetFloat("_ScanLineHeight", 0);
        Material2.SetFloat("_ScanLineHeight", 1);        
    }

    // Update is called once per frame
    protected override void Update()
    {
        //Check for user inputs
        base.Update();

        if (Play)
        {
            //Update timeline
            if (Direction)
                Timeline += Time.deltaTime * PlaySpeed;
            else
                Timeline -= Time.deltaTime * PlaySpeed;

            //Check for end of timeline
            if (Timeline >= 1)
            {
                Timeline = 1;
                Play = false;
                Direction = !Direction;
                UIObj.transform.Find("Option0").GetComponentInChildren<Toggle>().isOn = false;
            }
            else if (Timeline <= 0)
            {
                Timeline = 0;
                Play = false;
                Direction = !Direction;
                UIObj.transform.Find("Option0").GetComponentInChildren<Toggle>().isOn = false;
            }

            //Set material properties
            Material.SetFloat("_ScanLineHeight", Timeline);
            Material2.SetFloat("_ScanLineHeight", 1 - Timeline);
        }
    }

    protected override void ToggleBool()
    {
        //Omit calling base, as this toggle has unique functionality

        Play = true;

        UIObj.transform.Find("Option0").GetComponentInChildren<Toggle>().isOn = true;
    }
}
