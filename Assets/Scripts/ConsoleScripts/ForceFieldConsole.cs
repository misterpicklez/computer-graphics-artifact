﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimelineSegment
{
    public TimelineSegment(float pStartTime, float pEndTime, float pStartValue, float pEndValue)
    {
        startTime = pStartTime;
        endTime = pEndTime;
        startValue = pStartValue;
        endValue = pEndValue;
    }

    public float startTime;
    public float endTime;
    public float startValue;
    public float endValue;

    public float GetValueAtTime(float pTime)
    {
        if (pTime > endTime || pTime < startTime)
            return 0;

        //Find out how far along start->end time pTime is
        float t = Mathf.InverseLerp(startTime, endTime, pTime);        

        //lerp between start and end value using time
        return Mathf.Lerp(startValue, endValue, t);
    }
}

public class ForceFieldConsole : ConsoleScript
{
    public BoxCollider ForceFieldCollider;
    [Range(0, 2)]
    public float PlaySpeed = 1;
    private float Timeline = 0;
    private bool Play = false;
    private bool Direction = true; //true - go up, false - go down
    private List<TimelineSegment> EdgeThicknessTimeline = new List<TimelineSegment>();
    private List<TimelineSegment> InnerOpacityTimeline = new List<TimelineSegment>();

    // Start is called before the first frame update
    protected override void Start()
    {
        //Setup options for console        
        option tOption = new option(OptionType.E_BOOL, new Vector4(0, 0, 0, 0), new Vector4(0, 0, 0, 0), new Vector4(1, 0, 0, 0), "");
        options.Add(tOption);

        base.Start();
        

        //Initialize default material properties
        Material.SetVector("_Color", new Vector4(1, 0.0086f, 0, 1));
        Material.SetFloat("_EdgeThickness", 0.25f);
        Material.SetFloat("_InnerOpacity", 0.4f);

        //Setup timeline
        EdgeThicknessTimeline.Add(new TimelineSegment(0.0f , 0.5f , 0.25f, 0.0f));
        EdgeThicknessTimeline.Add(new TimelineSegment(0.5f , 1.0f , 0.0f , 0.0f));
        InnerOpacityTimeline.Add( new TimelineSegment(0.0f , 0.5f , 0.4f , 0.4f));
        InnerOpacityTimeline.Add( new TimelineSegment(0.5f , 1.0f , 0.4f , 0.0f));

        //Set UI toggle box to ticked by default
        UIObj.transform.Find("Option0").GetComponentInChildren<Toggle>().isOn = true;        
    }

    // Update is called once per frame
    protected override void Update()
    {
        //Check for user inputs
        base.Update();

        if (Play)
        {
            //Update timeline
            if (Direction)            
                Timeline += Time.deltaTime * PlaySpeed;            
            else            
                Timeline -= Time.deltaTime * PlaySpeed;

            //Check for end of timeline
            if (Timeline >= 1)
            {
                Timeline = 1;
                Play = false;
                Direction = !Direction;
                ForceFieldCollider.isTrigger = true;
                UIObj.transform.Find("Option0").GetComponentInChildren<Toggle>().isOn = false;
            }
            else if (Timeline <= 0)
            {
                Timeline = 0;
                Play = false;
                Direction = !Direction;
                UIObj.transform.Find("Option0").GetComponentInChildren<Toggle>().isOn = true;
            }

            //Set material properties
            Material.SetFloat("_EdgeThickness", GetTimelineValue(EdgeThicknessTimeline, Timeline));
            Material.SetFloat("_InnerOpacity", GetTimelineValue(InnerOpacityTimeline, Timeline));
        }
    }

    protected override void ToggleBool()
    {
        //Omit calling base, as this toggle has unique functionality

        Play = true;

        //If going from off->on, enable the box collider again
        if (Timeline >= 1)
            ForceFieldCollider.isTrigger = false;
    }

    protected float GetTimelineValue(List<TimelineSegment> TimelineSegments, float time)
    {
        float total = 0;
        //Sum up all timelines values (times outside a segment range return 0)
        for (int i = 0; i < TimelineSegments.Count; i++)
        {
            total += TimelineSegments[i].GetValueAtTime(time);
        }
        return total;
    }
}
