﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum OptionType
{
    E_NONE,
    E_BOOL,
    E_FLOAT,
    E_FLOAT4,
    E_MAX
}

public class ConsoleScript : MonoBehaviour
{
    public Material Material;
    public bool IsActive = false;
    [Range(0.001f, 1)]
    public float SliderSpeed = 0.005f;
    protected GameObject UIObj;
    protected bool toggleCooldown = false;
    protected float toggleTimer = .5f;

    //Each option has: a type, current value & corresponding material property
    [System.Serializable]
    public struct option
    {
        public OptionType type; 
        public Vector4 value;
        public Vector4 min;
        public Vector4 max;
        public string propertyName; 

        //Constructor
        public option(OptionType pType, Vector4 pValue, Vector4 pMin, Vector4 pMax, string pPropertyName)
        {
            this.type = pType;
            this.value = pValue;
            this.min = pMin;
            this.max = pMax;
            this.propertyName = pPropertyName;
        }
    };
    

    //Store a list of all options  
    [SerializeField]
    protected List<option> options = new List<option>();
    protected int CurrentSelection = 0;
    protected int CurrentChannel = 0;

    protected virtual void Start()
    {
        //highlight the first option    
        UIObj = GetComponentInChildren<Canvas>().gameObject;        
        UIObj.transform.Find("Option0").GetComponent<Text>().color = new Color(0.8f, 0, 0, 1);

        //Also highlight slider if float or float4 type
        ColorBlock colors;       
        if (options[0].type == OptionType.E_FLOAT)
        {
            colors = UIObj.transform.Find("Option0").GetComponentInChildren<Slider>().colors;
            colors.normalColor = new Color(0.8f, 0, 0, 1);
            UIObj.transform.Find("Option0").GetComponentInChildren<Slider>().colors = colors;
        }
        else if (options[0].type == OptionType.E_FLOAT4)
        {
            colors = UIObj.transform.Find("Option0").transform.Find("r-Slider").GetComponent<Slider>().colors;
            colors.normalColor = new Color(0.8f, 0, 0, 1);
            UIObj.transform.Find("Option0").transform.Find("r-Slider").GetComponent<Slider>().colors = colors;
        }




        //Set slider readouts
        for (int i = 0; i < options.Count; i++)
        {
            if (options[i].type == OptionType.E_BOOL)
            {
                if (options[i].value.x == 0)
                    SetToggle(false, i);
                else
                    SetToggle(true, i);
            }

            else if (options[i].type == OptionType.E_FLOAT)
            {
                SetSlider(options[i].value.x, i);
            }

            else if (options[i].type == OptionType.E_FLOAT4)
            {
                for (int ch = 0; ch < 4; ch++)
                {
                    SetSlider(options[i].value[ch], i, ch);
                }
            }
        }        
    }

    protected virtual void Update() 
    {
        if (IsActive)
        {
            //Receive players arrow key controls here
            if(Input.GetKeyDown(KeyCode.DownArrow))
            {
                MoveDown();
            }

            else if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                MoveUp();
            }

            if (!toggleCooldown)
            {
                if (Input.GetKey(KeyCode.RightArrow))
                {
                    MoveRight();
                }

                else if (Input.GetKey(KeyCode.LeftArrow))
                {
                    MoveLeft();
                }
            }                             
        }

        toggleTimer -= Time.deltaTime;
        if (toggleTimer <= 0)
        {
            toggleTimer = .5f;
            toggleCooldown = false;
        }                    
    }


    //############################
    //##### Arrow key inputs #####
    //############################
    protected virtual void MoveDown()
    {
        if (CurrentSelection < options.Count - 1)
        {
            //Check if its a 4-channel option selected
            if (options[CurrentSelection].type == OptionType.E_FLOAT4)
            {
                //Check if moving off the option
                if (CurrentChannel + 1 > 3)
                {
                    HighlightOption(Color.black, 3);
                    CurrentSelection++;
                    CurrentChannel = 0;
                    HighlightOption(new Color(0.8f, 0, 0, 1), 0);                    
                }

                //Else move down 1 channel
                else
                {
                    HighlightOption(Color.black, CurrentChannel);
                    CurrentChannel++;
                    HighlightOption(new Color(0.8f, 0, 0, 1), CurrentChannel);

                }
            }

            else
            {
                HighlightOption(new Color(0, 0, 0, 1));
                CurrentSelection++;
                CurrentChannel = options[CurrentSelection].type == OptionType.E_FLOAT4 ? (0) : (0);
                HighlightOption(new Color(0.8f, 0, 0, 1), CurrentChannel);                
            }
        }

        //Special case if first option is 4-channel
        else if (CurrentSelection == options.Count && options[CurrentSelection].type == OptionType.E_FLOAT4)
        {
            if (!(CurrentChannel + 1 > options.Count))
            {
                HighlightOption(Color.black, CurrentChannel);
                CurrentChannel++;
                HighlightOption(new Color(0.8f, 0, 0, 1), CurrentChannel);
            }
        }
    }
    protected virtual void MoveUp()
    {
        if (CurrentSelection > 0)
        {
            //Check if its a 4-channel option selected
            if (options[CurrentSelection].type == OptionType.E_FLOAT4)
            {
                //Check if moving off the option
                if (CurrentChannel - 1 < 0)
                {
                    HighlightOption(Color.black, 0);
                    CurrentSelection--;
                    CurrentChannel = 3;
                    HighlightOption(new Color(0.8f, 0, 0, 1));
                }

                //Else move up 1 channel
                else
                {
                    HighlightOption(Color.black, CurrentChannel);
                    CurrentChannel--;
                    HighlightOption(new Color(0.8f, 0, 0, 1), CurrentChannel);
                }
            }

            else
            {
                HighlightOption(new Color(0, 0, 0, 1));
                CurrentSelection--;
                CurrentChannel = options[CurrentSelection].type == OptionType.E_FLOAT4 ? (3) : (0);                
                HighlightOption(new Color(0.8f, 0, 0, 1), CurrentChannel);                
            }
        }

        //Special case if first option is 4-channel
        else if (CurrentSelection == 0 && options[CurrentSelection].type == OptionType.E_FLOAT4)
        {
            if (!(CurrentChannel - 1 < 0))
            {
                HighlightOption(Color.black, CurrentChannel);
                CurrentChannel--;
                HighlightOption(new Color(0.8f, 0, 0, 1), CurrentChannel);
            }
        }
    }
    protected void MoveRight()
    {
        switch (options[CurrentSelection].type)
        {
            case OptionType.E_BOOL:
                ToggleBool();
                toggleCooldown = true;
                break;
            case OptionType.E_FLOAT:
                MoveSlider(CurrentSelection, CurrentChannel, SliderSpeed);
                break;
            case OptionType.E_FLOAT4:
                MoveSlider(CurrentSelection, CurrentChannel, SliderSpeed);
                break;
        }
    }
    protected void MoveLeft()
    {
        switch (options[CurrentSelection].type)
        {
            case OptionType.E_BOOL:
                ToggleBool();
                toggleCooldown = true;
                break;
            case OptionType.E_FLOAT:
                MoveSlider(CurrentSelection, CurrentChannel, -SliderSpeed);            
                break;
            case OptionType.E_FLOAT4:
                MoveSlider(CurrentSelection, CurrentChannel, -SliderSpeed);
                break;
        }
    }


    //#####################################
    //##### Option response to inputs #####
    //#####################################
    protected virtual void ToggleBool()
    {
        //Base functionality: set toggle to on & pass this to material property        
        var tOption = options[CurrentSelection];        
        tOption.value.x = (tOption.value.x == 0) ? (tOption.value.x = 1) : (tOption.value.x = 0);
        options[CurrentSelection] = tOption;
        Material.SetFloat(tOption.propertyName, tOption.value.x);
        
        //Change togglebox's visuals
        if (tOption.value.x == 0)
            UIObj.transform.Find("Option" + CurrentSelection).GetComponentInChildren<Toggle>().isOn = false;
        else
            UIObj.transform.Find("Option" + CurrentSelection).GetComponentInChildren<Toggle>().isOn = true;

    }
    protected void MoveSlider(int option, int channel = 0, float percentIncrease = 0.01f)
    {
        if (options[option].type == OptionType.E_FLOAT)
        {
            float min = options[option].min.x;
            float max = options[option].max.x;
            float Change = (max - min) * percentIncrease;

            //Check change is not out of range
            float curValue = options[option].value.x;
            float newValue = curValue + Change;
            if (IsInRange(option, newValue))                            
                SetSlider(newValue, option); //If so, adjust slider value and pass new data to material

            //Else place the slider on the extreme value
            else
            {
                float extremeValue = (percentIncrease > 0) ? (max) : (min);
                SetSlider(extremeValue, option);
            }
        }
        else if (options[option].type == OptionType.E_FLOAT4)
        {
            float min = options[option].min[channel];
            float max = options[option].max[channel];
            float Change = (max - min) * percentIncrease;

            //Check change is not out of range
            float curValue = options[option].value[channel];
            float newValue = curValue + Change;
            if (IsInRange(option, newValue, channel))
                SetSlider(newValue, option, channel);

            else
            {
                float extremeValue = (percentIncrease > 0) ? (max) : (min);
                SetSlider(extremeValue, option, channel);
            }
        }


    }


    //###########################
    //##### Helper function #####
    //###########################
    protected void HighlightOption(Color color, int channel = 0)
    {
        //Change options text colour
        GameObject curOption = UIObj.transform.Find("Option" + CurrentSelection).gameObject;
        curOption.GetComponent<Text>().color = color;

        //Different highlighting depending on type
        ColorBlock colorblock;
        switch (options[CurrentSelection].type)
        {
            case OptionType.E_BOOL:
                //No extra work
                break;
            case OptionType.E_FLOAT:
                //Turn the slider handle red
                colorblock = curOption.GetComponentInChildren<Slider>().colors;
                colorblock.normalColor = color;
                curOption.GetComponentInChildren<Slider>().colors = colorblock;
                break;
            case OptionType.E_FLOAT4:
                //Turn the current channels slider handle red
                string[] channels = new string[4] { "r-Slider", "g-Slider", "b-Slider", "a-Slider" };
                string tChannel = channels[channel];
                colorblock = curOption.transform.Find(tChannel).GetComponentInChildren<Slider>().colors;
                colorblock.normalColor = color;
                curOption.transform.Find(tChannel).GetComponentInChildren<Slider>().colors = colorblock;
                break;
        }
    }
    protected void SetSlider(float value, int option, int channel = 0)
    {
        GameObject optionObj = UIObj.transform.Find("Option" + option).gameObject;

        //If 4-channel value
        if (options[option].type == OptionType.E_FLOAT4)
        {
            string[] channels = new string[4] { "r-Slider", "g-Slider", "b-Slider", "a-Slider" };
            string tChannel = channels[channel];

            //Move slider along
            optionObj.transform.Find(tChannel).GetComponent<Slider>().value = value;

            //Set value readout
            optionObj.transform.Find(tChannel).GetComponentInChildren<Text>().text = value.ToString();

            //Update console's value
            Vector4 newValue = options[option].value;
            newValue[channel] = value;
            option tOption = options[option];
            tOption.value = newValue;
            options[option] = tOption;

            //Get the 4-channel value & adjust current channel            
            Material.SetVector(options[option].propertyName, newValue);            
        }

        //If single channel value
        else if (options[option].type == OptionType.E_FLOAT)
        {
            //Move slider along
            optionObj.GetComponentInChildren<Slider>().value = value;

            //Set value readout            
            GameObject readoutObj = optionObj.transform.Find("Slider Value").gameObject;
            readoutObj.GetComponent<Text>().text = value.ToString();

            //Set material property
            Material.SetFloat(options[option].propertyName, value);

            //Update console's value
            option tOption = options[option];
            tOption.value.x = value;
            options[option] = tOption;
        }
    }
    protected void SetToggle(bool value, int option)
    {
        if (options[option].type == OptionType.E_BOOL)
        {
            GameObject toggleOption = UIObj.transform.Find("Option" + option).gameObject;

            if (value)
            {
                //Tick UI box
                toggleOption.GetComponentInChildren<Toggle>().isOn = true;

                //Set material property
                Material.SetFloat(options[option].propertyName, 1);
            }
            else
            {
                //Remove tick 
                toggleOption.GetComponentInChildren<Toggle>().isOn = false;

                //Set material property
                Material.SetFloat(options[option].propertyName, 0);
            }
        }
    }    
    protected bool IsInRange(int option, float newValue, int channel = 0)
    {
        if (options[option].type == OptionType.E_FLOAT)
        {
            float min = options[option].min.x;
            float max = options[option].max.x;
            if (newValue >= min && newValue <= max)
                return true;
        }
        else if (options[option].type == OptionType.E_FLOAT4)
        {
            float min = options[option].min[channel];
            float max = options[option].max[channel];
            if (newValue >= min && newValue <= max)
                return true;
        }

        return false;
    }
}
