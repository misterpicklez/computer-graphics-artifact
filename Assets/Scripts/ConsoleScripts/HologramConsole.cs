﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HologramConsole : ConsoleScript
{
    
    protected override void MoveDown()
    {
        //Call base  move down functionality
        base.MoveDown();

        CheckPage();
    }
    protected override void MoveUp()
    {        
        base.MoveUp();

        CheckPage();
    }

    //###########################
    //##### Helper function #####
    //###########################
    protected void CheckPage()
    {
        //Check if the current selection is on second page
        if (CurrentSelection >= 4)
        {
            HidePage(1);
            ShowPage(2);
            //Position and make arrow face upwards
            UIObj.transform.Find("NextPageIndicator").transform.localRotation = Quaternion.Euler(0, 0, 180);
            Vector3 pos = UIObj.transform.Find("NextPageIndicator").transform.localPosition;
            pos.y = 350;
            UIObj.transform.Find("NextPageIndicator").transform.localPosition = pos; 
        }
        else if (CurrentSelection <= 3)
        {
            HidePage(2);
            ShowPage(1);
            //Position and make arrow face downwards
            UIObj.transform.Find("NextPageIndicator").transform.localRotation = Quaternion.Euler(0, 0, 0);
            Vector3 pos = UIObj.transform.Find("NextPageIndicator").transform.localPosition;
            pos.y = -350;
            UIObj.transform.Find("NextPageIndicator").transform.localPosition = pos;
        }
    }
    protected void HidePage(int page)
    {
        if (page == 1)
        {
            UIObj.transform.Find("Option0").gameObject.SetActive(false);
            UIObj.transform.Find("Option1").gameObject.SetActive(false);
            UIObj.transform.Find("Option2").gameObject.SetActive(false);
            UIObj.transform.Find("Option3").gameObject.SetActive(false);
        }
        else if (page == 2)
        {
            UIObj.transform.Find("Option4").gameObject.SetActive(false);
            UIObj.transform.Find("Option5").gameObject.SetActive(false);
            UIObj.transform.Find("Option6").gameObject.SetActive(false);
            UIObj.transform.Find("Option7").gameObject.SetActive(false);
        }        
    }
    protected void ShowPage(int page)
    {
        if (page == 1)
        {
            UIObj.transform.Find("Option0").gameObject.SetActive(true);
            UIObj.transform.Find("Option1").gameObject.SetActive(true);
            UIObj.transform.Find("Option2").gameObject.SetActive(true);
            UIObj.transform.Find("Option3").gameObject.SetActive(true);
        }
        else if (page == 2)
        {
            UIObj.transform.Find("Option4").gameObject.SetActive(true);
            UIObj.transform.Find("Option5").gameObject.SetActive(true);
            UIObj.transform.Find("Option6").gameObject.SetActive(true);
            UIObj.transform.Find("Option7").gameObject.SetActive(true);
        }
    }
}
