﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public Camera playerCam;
    public CharacterController controller;
    public Transform groundCheck;
    public float speed = 12f;
    public float gravity = -9.81f;
    public float groundDist = 0.2f;
    public LayerMask groundMask;

    Vector3 velocity;
    bool isGrounded;
    ConsoleScript activeConsole;

    bool lockMovement = false;

    // Update is called once per frame
    void Update()
    {
        //#########################
        //-----MOVEMENT UPDATE-----
        //#########################
        if (!lockMovement)
        {
            isGrounded = Physics.CheckSphere(groundCheck.position, 0.01f, groundMask);

            if (isGrounded && velocity.y < 0)
            {
                velocity.y = -2f;
            }

            float x = Input.GetAxis("Horizontal");
            float z = Input.GetAxis("Vertical");

            Vector3 move = (transform.right * x) +
                           (transform.forward * z);

            controller.Move(move * speed * Time.deltaTime);

            velocity.y += gravity * Time.deltaTime;

            controller.Move(velocity * Time.deltaTime);
        }

        //############################
        //-----INTERACTION UPDATE-----
        //############################
        if (Input.GetKeyUp("e"))
        {            
            if (!lockMovement)
            { 
                Vector3 start = playerCam.transform.position;
                Vector3 dir = playerCam.transform.forward;
                float dist = 3;
                int consoleLayer = 9; //Console layer mask

                //Raycast from player pov & look for console
                RaycastHit hit;
                if (Physics.Raycast(start, dir, out hit, dist) && hit.collider.gameObject.layer == consoleLayer)
                {
                    //Console is found, get ref to it
                    ConsoleScript tConsole = hit.collider.gameObject.GetComponent<ConsoleScript>();

                    //Set active console
                    tConsole.IsActive = true;
                    activeConsole = tConsole;
                    lockMovement = true;
                    Debug.LogError("Hit console" + tConsole);
                }
                Debug.DrawRay(start, dir * dist, Color.red, 10);
            }
            else
            {
                activeConsole.IsActive = false;
                activeConsole = null;
                lockMovement = false;
                Debug.Log("Leaving console" + activeConsole);
            }
        }
    }
}
